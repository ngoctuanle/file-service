FROM gradle:8.2.1-jdk17 as builder
RUN mkdir /app
WORKDIR /app
COPY . .
RUN gradle clean build -x test

FROM --platform=linux/amd64 gcr.io/distroless/java17-debian11
WORKDIR /project
COPY --from=builder /app/build/libs/file-service-0.0.1-SNAPSHOT.jar app.jar
COPY HELP.md files/
ENTRYPOINT ["java", "-jar", "app.jar"]