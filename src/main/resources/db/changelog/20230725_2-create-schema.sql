--liquibase formatted sql
--changeset microservice_class:20230725_1

CREATE
EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE FILE_UPLOAD
(
    ID         uuid PRIMARY KEY default uuid_generate_v4(),
    FILENAME    varchar(128),
    FULLPATH    varchar(500),
    FILETYPE    varchar(128)
);