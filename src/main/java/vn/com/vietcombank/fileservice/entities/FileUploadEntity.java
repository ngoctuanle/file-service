package vn.com.vietcombank.fileservice.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Entity
@Table(name = "FILE_UPLOAD")
@Getter
@Setter
public class FileUploadEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private UUID id;

    @Column(name = "FILENAME", nullable = false, length = 128)
    private String fileName;

    @Column(name = "FULLPATH", nullable = false, length = 500)
    private String fullPath;

    @Column(name = "FILETYPE", nullable = false, length = 128)
    private String contentType;
}
