package vn.com.vietcombank.fileservice.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vn.com.vietcombank.fileservice.entities.FileUploadEntity;
import vn.com.vietcombank.fileservice.interfaces.IFileUploadService;

import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/api/v1/file")
public class FileUploadController {
    @Autowired
    IFileUploadService fileUploadService;

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public UUID upload(@RequestParam("file") MultipartFile file) {
        return fileUploadService.saveFile(file);
    }

    @RequestMapping(value =  "/delete/{fileId}", method = RequestMethod.GET)
    public boolean delete(@PathVariable("fileId") UUID fileId) {
        return fileUploadService.deleteFile(fileId);
    }

    @RequestMapping(value =  "/get/{fileId}", method = RequestMethod.GET)
    public FileUploadEntity get(@PathVariable("fileId") UUID fileId) {
        return fileUploadService.getById(fileId);
    }
}
