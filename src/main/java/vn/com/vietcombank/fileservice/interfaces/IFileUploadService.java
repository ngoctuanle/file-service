package vn.com.vietcombank.fileservice.interfaces;

import org.springframework.web.multipart.MultipartFile;
import vn.com.vietcombank.fileservice.entities.FileUploadEntity;
import java.util.UUID;

public interface IFileUploadService {
    public UUID saveFile(MultipartFile file);

    public boolean deleteFile(UUID fileId);

    public FileUploadEntity getById(UUID fileId);
}
