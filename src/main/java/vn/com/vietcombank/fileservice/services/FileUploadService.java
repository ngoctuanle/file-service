package vn.com.vietcombank.fileservice.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import vn.com.vietcombank.fileservice.entities.FileUploadEntity;
import vn.com.vietcombank.fileservice.interfaces.IFileUploadService;
import vn.com.vietcombank.fileservice.repositories.FileUploadRepository;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Optional;
import java.util.UUID;

@Service("FileUploadService")
public class FileUploadService implements IFileUploadService {
    @Value("${upload-file.dir}")
    private String UPLOAD_DIR;

    @Value("${upload-file.max-size}")
    private Long MAX_FILE_SIZE = 0L;

    @Autowired
    FileUploadRepository fileUploadRepository;

    @Override
    public UUID saveFile(MultipartFile file) {
        try {
            String fileName = file.getOriginalFilename();
            String destinationFilePath = UPLOAD_DIR + fileName;

            if (file.getSize() / 1024 > MAX_FILE_SIZE) {
                return null;
            }

            Files.copy(file.getInputStream(), Paths.get(destinationFilePath), StandardCopyOption.REPLACE_EXISTING);

            FileUploadEntity fileUploadEntity = new FileUploadEntity();
            fileUploadEntity.setFileName(fileName);
            fileUploadEntity.setFullPath(destinationFilePath);
            fileUploadEntity.setContentType(file.getContentType());
            fileUploadEntity = fileUploadRepository.save(fileUploadEntity);
            return fileUploadEntity.getId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteFile(UUID fileId) {
        try {
            Optional<FileUploadEntity> optEntity = fileUploadRepository.findById(fileId);
            if(optEntity.isPresent()) {
                FileUploadEntity entity = optEntity.get();
                Files.delete(Paths.get(entity.getFullPath()));
                fileUploadRepository.deleteById(fileId);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public FileUploadEntity getById(UUID fileId) {
        Optional<FileUploadEntity> optionalFileUploadEntity = fileUploadRepository.findById(fileId);
        return optionalFileUploadEntity.orElse(null);

    }
}
